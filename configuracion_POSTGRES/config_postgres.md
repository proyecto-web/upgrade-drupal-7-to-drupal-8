Se busca postgres 11 para la intalación
```
sudo apt-cache search postgresql-11
```

Se instala la versión 11 de Postgresql
```
apt install postgresql libpq5 postgresql-11 postgresql-client-11 postgresql-client-common postgresql-contrib

pg_ctlcluster 11 main start
```

Se verifica que el servicio este corriendo
```
systemctl status postgresql
```

Se intalan los Modulos adicionales para Apache2, Postgresql v11 y Drupal
```
apt install -y php5.6-pgsql php5.6-dom php5.6-gd php5.6-json php5.6-pdo php5.6-simplexml php5.6-tokenizer php5.6-xml 
```

Antes de cambiar la configuración de postgres necesaria para el funciomamiento de los sitios, se establece un password al usuario postgres, nos conectamos a postgres con *psql* y luego le asignamos un password
```
psql

\password postgres
```
Cambiamos al usuario postgres para poder crear nuestras bases de datos y el usuario
```
sudo -i -u postgres
```
Creamos el usuario **becario**, este es el que se encargará de administrar la base de datos de nuestro drupal.
```
createuser --pwprompt --encrypted --no-createrole --no-createdb becario
```
Se crean las bases de datos de drupal con el usuario becario como dueño
```
createdb --encoding=UTF8 --owner=becario sitio1_db
createdb --encoding=UTF8 --owner=becario sitio2_db
createdb --encoding=UTF8 --owner=becario sitio1_8db
createdb --encoding=UTF8 --owner=becario sitio2_8db
```
Entramos a nuestas bases de datos para cambiar del schema _public_ a un schema que crearemos para drupal. Este paso se realiza para cada una de las bases de datos creadas.
```
Nos conectamos a nuestras bases de datos con nuestro usuario.
psql -U becario -d sitio1_db
psql -U becario -d sitio2_db
psql -U becario -d sitio1_8db
psql -U becario -d sitio2_8db
```

Dentro de nuestra base de datos se crea un nuevo eschema para en la base de datos, de esta manera se cambia el schema default que es public.
```
Creamos el schema
CREATE SCHEMA drupal AUTHORIZATION becario;

Se establece schema como principal
SET search_path TO drupal;

Podemos ver el schema que se utiliza ahora
SELECT current_schema();
```

Cambio en el archivo *pg_hba.conf*, deshabilitamos IPv6 ya que no se requiere.
```
# Database administrative login by Unix domain socket
local   all             postgres                                md5

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
#host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
#host    replication     all             ::1/128                 md5
```

Permitir el acceso al usuario becario a la base de datos de los dos sitios desde la ip de la máquina *192.168.100.101* en el archivo pg_hba.conf.
```
host    sitio1_db       becario         192.168.100.101/24      md5
host    sitio2_db       becario         192.168.100.101/24      md5
host    sitio1_d8_db    becario         192.168.100.101/24      md5
host    sitio2_d8_db    becario         192.168.100.101/24      md5
host    sitio1_d8_db    becario         192.168.100.197/24      md5
host    sitio2_d8_db    becario         192.168.100.197/24      md5
```
```
# Database administrative login by Unix domain socket
local   all             postgres                                md5

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
host	prueba		    becario		    127.0.0.1/32		    md5
host	sitio1_db	    becario		    192.168.100.101/24	    md5
host	sitio2_db	    becario		    192.168.100.101/24	    md5
host    sitio1_d8_db    becario         192.168.100.101/24      md5
host    sitio2_d8_db    becario         192.168.100.101/24      md5
host    sitio1_d8_db    becario         192.168.100.197/24      md5
host    sitio2_d8_db    becario         192.168.100.197/24      md5
# IPv6 local connections:
#host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
#host    replication     all             ::1/128                 md5
```

Para la creación de las nuevas bases de datos de igual manera se permite la conexión al usuario becario, estas bases de datos son creadas al momento de migrar el drupal que se encuentra en debian 8 a debian 10.

```
# Database administrative login by Unix domain socket
local   all             postgres                                md5

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
host	prueba		    becario		    127.0.0.1/32		    md5
host	sitio1_db	    becario		    192.168.100.101/24	    md5
host	sitio2_db	    becario		    192.168.100.101/24	    md5
host    sitio1_d8_db    becario         192.168.100.101/24      md5
host    sitio2_d8_db    becario         192.168.100.101/24      md5
host    sitio1_d8_db    becario         192.168.100.197/24      md5
host    sitio2_d8_db    becario         192.168.100.197/24      md5
# IPv6 local connections:
#host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
#host    replication     all             ::1/128                 md5
```
