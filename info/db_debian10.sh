###############################
# Instalación base en debian 10 web server

# Instalando postgres
sudo apt install -y vim wget
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
RELEASE=$(lsb_release -cs)
echo "deb http://apt.postgresql.org/pub/repos/apt/ ${RELEASE}"-pgdg main | sudo tee  /etc/apt/sources.list.d/pgdg.list

sudo apt update
sudo apt -y install postgresql-11
sudo apt -y install php
sudo apt -y install php-pgsql


# Verificamos
# systemctl status postgresql

# Creamos usuario y BD
sudo su postgres
createuser --pwprompt --encrypted --no-createrole --no-createdb admin_sitio1

createdb --encoding=UTF8 --owner=admin_sitio1 sitio1_DB


# 192.168.88.134