#!/bin/bash

# Corrobora que se está corriendo el script como root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

##############################################################
##############################################################

# Certificados de seguridad
# openssl req -x509 -newkey rsa:4096 -keyout sitio1.key -out sitio1.pem -days 365 -nodes
# openssl req -x509 -newkey rsa:4096 -keyout sitio2.key -out sitio2.pem -days 365 -nodes

# sudo mv sitio1.key /etc/ssl/private/
# sudo mv sitio2.key /etc/ssl/private/

# sudo mv sitio1.pem /etc/ssl/certs/
# sudo mv sitio2.pem /etc/ssl/certs/

##############################################################
##############################################################

# Instalación de PHP

sudo apt -y install ca-certificates apt-transport-https 
wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
sudo echo "deb https://packages.sury.org/php/ jessie main" | sudo tee /etc/apt/sources.list.d/php.list

sudo apt -y install php7.2 php7.2-cli php7.2-common php7.2-mbstring php7.2-gd php7.2-pgsql php7.2-xml php7.2-ldap 


##############################################################
##############################################################
# Instalación base en debian 8 web server

# Instalación de apache
sudo apt-get install apache2

# Activando módulos Apache
sudo a2enmod ssl
sudo a2enmod rewrite

# Configuración de sitio1.conf
# sudo vim /etc/apache2/sites-available/sitio1.conf
sudo cp sitio1.conf /etc/apache2/sites-available/
# Añadir contenido sitio1.conf
# sudo vim /etc/apache2/sites-available/sitio2.conf
sudo cp sitio2.conf /etc/apache2/sites-available/
# Añadir contenido sitio2.conf

sudo a2ensite sitio1.conf
sudo a2ensite sitio2.conf

sudo systemctl restart apache2


##############################################################
##############################################################

# Carpetas

sudo mkdir /var/www/sitio1
sudo mkdir /var/www/sitio2

# Configurar /etc/hosts
sudo vim /etc/hosts
# 127.0.1.2       sitio1.cert.unam.mx www.sitio1.cert.unam.mx

##############################################################
##############################################################

# Instalando postgres
sudo echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" >> /etc/apt/sources.list 
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - 
sudo apt-get update 
apt-get install postgresql-11 -y 

##############################################################
##############################################################

# Drush

sudo apt-get -y install git curl

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Para verificar
# composer --version

wget https://github.com/drush-ops/drush/releases/download/8.0.1/drush.phar
# Verificación 
# php drush.phar core-status
chmod +x drush.phar
sudo mv drush.phar /usr/local/bin/drush
drush init

# Verificación drush
# drush

##############################################################
##############################################################

# Sitio 1

sudo cd /var/www/sitio1/
sudo drush dl drupal-7.62
# Selecciona la versión estable

# Descomprime y mueve todo para que quede en /var/www/sitio1
sudo mv drupal-7.69/* ./
sudo rm -r drupal-7.69/

sudo chown -R www-data:www-data /var/www/sitio1
sudo chown -R www-data:www-data /var/www/sitio2

##############################################################
##############################################################

# Sitio 2

sudo cd /var/www/sitio2/
sudo drush dl drupal-7.69
# Selecciona la versión estable

# Descomprime y mueve todo para que quede en /var/www/sitio2
sudo mv drupal-7.69/* ./
sudo rm -r drupal-7.69/

sudo chown -R www-data:www-data /var/www/sitio1
sudo chown -R www-data:www-data /var/www/sitio2

# drush site-install --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio2_db --account-name=admin_sitio2 --account-pass=hola123.,