###############################
# Instalación base en debian 8 web server

# Instalación de apache
sudo apt-get install apache2

# Instalación de PHP

sudo apt -y install lsb-release apt-transport-https ca-certificates 

sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg

echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.3.list

# Activando módulos

sudo a2enmod ssl
sudo a2enmod rewrite
sudo systemctl restart apache2

# Crea la carpeta

sudo mkdir /var/www/sitio1
sudo mkdir /var/www/sitio2

# Crea certificados de seguridad
openssl req -x509 -newkey rsa:4096 -keyout sitio1.key -out sitio1.pem -days 365 -nodes
openssl req -x509 -newkey rsa:4096 -keyout sitio2.key -out sitio2.pem -days 365 -nodes

sudo mv sitio1.key /etc/ssl/private/
sudo mv sitio2.key /etc/ssl/private/

sudo mv sitio1.pem /etc/ssl/certs/
sudo mv sitio2.pem /etc/ssl/certs/


# Configuración de sitio1.conf
sudo vim /etc/apache2/sites-available/sitio1.conf
# Añadir contenido sitio1.conf
sudo vim /etc/apache2/sites-available/sitio2.conf
# Añadir contenido sitio2.conf

sudo a2ensite sitio1.conf
sudo a2ensite sitio2.conf

sudo systemctl restart apache2

# Configurar /etc/hosts
sudo vim /etc/hosts
127.0.1.2       sitio1.cert.unam.mx www.sitio1.cert.unam.mx

# Instalando drush
sudo apt-get update
sudo apt-get install git -y
sudo apt-get install curl -y
sudo apt-get install php7.2-xml -y

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Para verificarphp
# composer --version

wget https://github.com/drush-ops/drush/releases/download/8.0.1/drush.phar
# Verificación 
# php drush.phar core-status
chmod +x drush.phar
sudo mv drush.phar /usr/local/bin/drush
drush init

# Verificación drush
# drush

cd /var/www/sitio1/
sudo drush dl drupal-7 --select
# Selecciona la versión estable

# Descomprime y mueve todo para que quede en /var/www/sitio1

sudo apt update

sudo apt -y install php7.2
sudo apt -y install php7.2-pgsql
sudo apt -y install php7.2-gd
sudo apt -y install php7.2-mbstring
sudo apt-get -y install php7.2 php7.2-cli php7.2-common