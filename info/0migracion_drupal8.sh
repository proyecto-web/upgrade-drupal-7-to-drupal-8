##############################################################
##############################################################

##############################################################
##############################################################

# Respaldo
mkdir respaldo
cp /etc/apache2/sites-available/sitio1.conf ./respaldo/
cp /etc/apache2/sites-available/sitio2.conf ./respaldo/
cp -r /var/www/sitio1 ./respaldo/
cp -r /var/www/sitio2 ./respaldo/

rm -r sitio1/
cd sitio1

# PHP 7.2
sudo apt -y install php7.2 php7.2-cli php7.2-common php7.2-mbstring php7.2-gd php7.2-pgsql php7.2-xml php7.2-ldap 

sudo update-alternatives --set php /usr/bin/php7.3


# Instalación limpia
sudo drush dl drupal-8 --select

mv drupal-8.8.4/* ./
rm -r drupal-8.8.4/ 

# drush dl --drupal-project-rename=sitio1

drush site-install --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio1_8db --account-name=admin_sitio1 --account-pass=hola123.,


drush si standard --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio1_8db --account-name=admin_sitio1 --account-pass=hola123.,

drush si standard --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio1_8db --db-su=root --db-su-pw=root --site-name="Drupal on Vagrant"


drush si standard --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio1_8db --site-name="Sitio1"

drush si standard --db-url=mysql://root:root@localhost/drupal7 --db-su=root --db-su-pw=root --site-name="Drupal on Vagrant"


# drush sql:create --db-su=root --db-su-pw=rootpassword --db-url="pgsql://becario:hola123.,@192.168.100.100/sitio1_8db"





# Plugins necesarios
# Drush

sudo apt-get -y install git curl

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Para verificar
# composer --version

wget https://github.com/drush-ops/drush/releases/download/8.0.1/drush.phar
# Verificación 
# php drush.phar core-status
chmod +x drush.phar
sudo mv drush.phar /usr/local/bin/drush
drush init

# Verificación drush
# drush

##############################################################
##############################################################

# Sitio 1

sudo cd /var/www/sitio1/
sudo drush dl drupal-7.62
# Selecciona la versión estable

# Descomprime y mueve todo para que quede en /var/www/sitio1
sudo mv drupal-7.69/* ./
sudo rm -r drupal-7.69/

sudo chown -R www-data:www-data /var/www/sitio1
sudo chown -R www-data:www-data /var/www/sitio2

##############################################################
##############################################################

# Sitio 2

sudo cd /var/www/sitio2/
sudo drush dl drupal-7.69
# Selecciona la versión estable

# Descomprime y mueve todo para que quede en /var/www/sitio2
sudo mv drupal-7.69/* ./
sudo rm -r drupal-7.69/

sudo chown -R www-data:www-data /var/www/sitio1
sudo chown -R www-data:www-data /var/www/sitio2

drush site-install --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio2_db --account-name=admin_sitio2 --account-pass=hola123.,