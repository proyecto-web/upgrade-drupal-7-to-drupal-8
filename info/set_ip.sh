#!/bin/bash

# Corrobora que se está corriendo el script como root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Set Your IP Address
ifconfig eth0 192.168.100.200 netmask 255.255.255.0 up

# Set Your Default Gateway
route add default gw 192.168.100.1

# Set Your DNS Server
echo "nameserver 1.1.1.1" > /etc/resolv.conf

# Desactivar IPV6
sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1
sysctl -w net.ipv6.conf.lo.disable_ipv6=1