 #!/bin/bash

##############################################################
##############################################################
USER=user 
PW=hola123.,
IP_DESTINO=192.168.100.197
CERT_PEM_1=sitio1.pem
CERT_KEY_1=sitio1.key
CERT_PEM_2=sitio2.pem
CERT_KEY_2=sitio2.key
CONFIG_APACHE1=sitio1.conf
CONFIG_APACHE2=sitio2.conf

echo "> Bitacora. " >> bitacora.log
echo "> Iniciando el respaldo. " >> bitacora.log
echo "> Iniciando el respaldo. "

# Respaldo
mkdir respaldo
cp /etc/apache2/sites-available/$CONFIG_APACHE1 ./respaldo/ 2>> bitacora.log
cp /etc/apache2/sites-available/$CONFIG_APACHE2 ./respaldo/ 2>> bitacora.log
cp /etc/ssl/certs/$CERT_PEM_1 ./respaldo 2>> bitacora.log
cp /etc/ssl/private/$CERT_KEY_1 ./respaldo 2>> bitacora.log
cp /etc/ssl/certs/$CERT_PEM_2 ./respaldo 2>> bitacora.log
cp /etc/ssl/private/$CERT_KEY_2 ./respaldo 2>> bitacora.log
cp -r /var/www/sitio1 ./respaldo/ 2>> bitacora.log
cp -r /var/www/sitio2 ./respaldo/ 2>> bitacora.log
# Configs seguras
cp -r ./configuraciones_seguras ./respaldo/ 2>> bitacora.log
cp ./2install_sites.sh ./respaldo/ 2>> bitacora.log


echo "> Archivos copiados. " >> bitacora.log
echo "> Archivos copiados. "

echo "> Generando tar. " >> bitacora.log
echo "> Generando tar. "

tar -cvzf respaldo.tar.gz respaldo 2>> bitacora.log

echo "> Tar generado. " >> bitacora.log
echo "> Tar generado. "

echo "> Transfiriendo tar. " >> bitacora.log
echo "> Transfiriendo tar. "

scp ./respaldo.tar.gz $USER@$IP_DESTINO:/home/$USER 2>> bitacora.log

echo "> Tar transferido. " >> bitacora.log
echo "> Tar transferido. "

echo "> Corrobore la bitácora en busca de errores"
echo "> "
echo "> Si todo ha salido bien ingrese a $IP_DESTINO para proseguir con la migración" 