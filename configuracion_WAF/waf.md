# Configuración WAF (proxy inverso)

## Instalamos apache2
````sudo apt install apache2````

## Iniciamos el servicio de apache
````sudo systemctl start apache2````

## Habilitamos el servicio de apache para que al reiniciar el equipo se inicie automaticamente el servicio de apache
````sudo systemctl enable apache2````

## Habilitamos los modulos de apache para el proxy
````
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_ftp
sudo a2enmod proxy_htmlV
sudo a2enmod proxy_ajp
sudo a2enmod headers
````
## Nos dirijimos a los archivos de configuracion de virtualhost
````cd /etc/apache2/sites-available````

## Creamos un archivo de configuración para el proxy
````sudo vim proxy-sitio1.conf````

## Debe de quedar de la siguiente manera
````
<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                ServerName sitio1.cert.unam.mx
                ServerAdmin becarios@bec.seguridad.unam.mx

                ErrorLog ${APACHE_LOG_DIR}/sitio1-error.log
                Customlog ${APACHE_LOG_DIR}/sitio1-access.log combined

                SSLProxyEngine On
                SSLProxyVerify none

                SSLCertificateFile /etc/ssl/certs/sitio1.pem
                SSLCertificateKeyFile /etc/ssl/private/sitio1.key

                ProxyPass / https://sitio1.cert.unam.mx:443/
                ProxyPassReverse / https://sitio1.cert.unam.mx:443/

                SecRuleEngine On
                SecRule ARGS:testparam "@contains test" "id:1234, status:403, msg:'Prueba'"

                <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
                </FilesMatch>

                <Directory /usr/lib/cgi_bin>
                        SSLOptions +StdEnvVars
                </Directory>

                BrowserMatch "MSIE [2-6]" \
                        nokeepalive ssl-unclean-shutdown \
                        downgrade-1.0 force-response-1.0
                BrowserMatch "MSIE [17.9]" ssl-unclean-shutdown
        </VirtualHost>
</IfModule>
<VirtualHost *:80>
        ServerName sitio1.cert.unam.mx
        ServerAdmin becarios@bec.seguridad.unam.mx

        ProxyRequests Off
        ProxyPreserveHost On
        ProxyPass / http://sitio1.cert.unam.mx:8080/
        ProxyPassReverse / http://sitio1.cert.unam.mx:8080/
        ProxyHTMLURLMap http://sitio1.cert.unam.mx
        #Redirect permanent / https://sitio1.cert.unam.mx

        SecRuleEngine On
        SecRule ARGS:testparam "@contains test" "id:1234, status:403, msg:'Prueba'"
        SSLProxyEngine On
        SSLProxyVerify none

        <Directory /var/www>
                Options -Indexes
                AllowOverride
                Require all granted
                RewriteEngine On
                RewriteBase /
                RewriteRule ^(.*)$ "https://sitio1.cert.unam.mx" [L]
        </Directory>

        <Proxy /var/www>
                Order deny,allow
                Deny from all
                Allow from all
        </Proxy>

        <Location /var/www>
                Deny from all
                Redirect https://sitio1.cert.unam.mx
                Allow from all
                ProxyHTMLEnable On
                RequestHeader unset Accept-Encoding
        </Location>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
````
## Habilitamos el sitio
````sudo a2ensite proxy-sitio1````

## Reiniciamos el servicio de apache
````sudo systemctl restart apache2````


Se realizara el mismo proceso de realizar un archivo de configuración para el sitio 2 con el nombre de proxy-sitio2.conf en donde podremos 
copiar el archivo de configuración ya antes realizado y cambiar las siguientes lineas:
````
<VirtualHost _default_:443>
        ServerName sitio2.cert.unam.mx
        ErrorLog ${APACHE_LOG_DIR}/sitio2-error.log
        Customlog ${APACHE_LOG_DIR}/sitio2-access.log combined
        SSLCertificateFile /etc/ssl/certs/sitio2.pem
        SSLCertificateKeyFile /etc/ssl/private/sitio2.key
        ProxyPass / https://sitio2.cert.unam.mx:443/
        ProxyPassReverse / https://sitio2.cert.unam.mx:443/
</VirtualHost>
<VirtualHost *:80>
        ServerName sitio2.cert.unam.mx
        ProxyPass / http://sitio2.cert.unam.mx:8080/
        ProxyPassReverse / http://sitio2.cert.unam.mx:8080/
        ProxyHTMLURLMap http://sitio2.cert.unam.mx
        <Directory /var/www>
                RewriteRule ^(.*)$ "https://sitio1.cert.unam.mx" [L]
        </Directory>
        <Location /var/www>
                Redirect https://sitio1.cert.unam.mx
        </Location>
</VirtualHost>
````

Una vez realizado los cambios en el archivo de configuración del sitio 2, procederemos a activar el sitio con ````sudo a2ensite proxy-sitio2.conf````

Ya que tenemos el sitio activado procederemos a reiniciar el servicio de apache.

# Configuración de modsecurity

## Instalamos git 
````sudo apt install git````

## Instalamos modsecurituy para apache
````sudo apt install libapache2-modsecurity````


Una vez instalado procederemos a configurar modsecurity

## Crearemos una copia del archivo de configuración de modsecurity
````sudo cp /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf````

## Editamos el archivo de configuracón para realizar los siguiente cambios
````sudo vim /etc/modsecurity/modsecurity.conf````

Cambiamos la siguiente linea:
````
SecRuleEngine On ---> SecRuleEngine DetectionOnly
````

## Eliminaremos el archivo modsecurity-crs que se crea por default
````sudo rm -rf /usr/share/modsecurity-crs````

## Descargaremos la version más reciente de CRS
````sudo git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git /usr/share/modsecurity-crs````

## Procederemos a editar el archivo de security2.conf para activar las reglas y añadimos las siguientes lineas 
````sudo vim /etc/apache2/mods-enabled/security2.conf````
````
<IfModule security2_module>

        SecDataDir /var/cache/modsecurity

        IncludeOptional /etc/modsecurity/*.conf

        IncludeOptional “/usr/share/modsecurity-crs/*.conf

        IncludeOptional “/usr/share/modsecurity-crs/rules/*.conf

</IfModule>
````

## Reiniciamos apache y la configuración quedara lista
```` sudo systemctl restart apache2````

El ultimo paso ser añadir en nuestro ``/etc/hosts/`` la IP del servidor web 