# Verificamos permisos
cd /var/www/sitio1
sudo find . -type d -exec chmod u=rwx,g=rx,o= '{}' \;
sudo find . -type f -exec chmod u=rw,g=r,o= '{}' \;
sudo chown -R user:www-data *

# Modulos core

# Term Search
drush en term_search -y

# Chaos Tool Suite
drush en ctools -y

# CSS Editor
drush en css_editor -y

# Date/Time
drush en date -y

# Event Log (Menu, Node, Taxonomy)
drush en event_log -y

# Feeds
drush en feeds -y

# Fields
# Viene por defecto

# File Management
drush en file_manager -y

# Google Analytics
# drush en google_analytics -y 
# Viene por defecto

# Workflow (API, Node, Notify)
drush en workflow -y

drush en workflow_access -y 
drush en workflow_vbo -y 
drush en workflow -y 
drush en workflow_cleanup -y 
drush en workflowfield -y 
drush en workflownode -y 
drush en workflow_search_api -y 
drush en workflow_notify -y 
drush en workflow_notify_og -y 
drush en workflow_revert -y 
drush en workflow_rules -y 
drush en workflow_actions -y 
drush en workflow_admin_ui -y 
drush en workflow_views -y 

# Input Filters
# No disponible

# CKEditor
drush en ckeditor -y 

# Gallery Formatter
drush en galleryformatter -y 

# JQuery Update
drush en jquery_update -y

# Popup Message
drush en popup_message -y

# Submenu Tree
drush en submenutree -y

# Simple LDAP (User)
drush en simple_ldap -y

# SMTP Authentication Support
drush en smtp -y

# Internationalization
drush en i18n -y

# String Translation
# drush en i18n_strings -y
drush en i18n_string -y

# Site Map
drush en site_map -y

# Token
drush en token -y

# Panels
drush en panels -y

# Mini Panels
drush en panels_mini_ipe -y

# Rules
drush en rules -y

# Metatag (Panels)
drush en metatag -y

# CAPTCHA
drush en captcha -y

# Variable
drush en variable -y

# Views
drush en views -y

# Tema responsivo (Elección del equipo)
#drush en breakpoints -y 
drush en impact_theme -y
