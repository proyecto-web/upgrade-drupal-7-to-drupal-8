#!/bin/bash

# Corrobora que se está corriendo el script como root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

##############################################################
##############################################################

# Instalación de PHP

echo "> Bitacora. " >> bitacora.log
echo "> Iniciando instalación. " >> bitacora.log
echo "> Iniciando instalación. "

apt -y install ca-certificates apt-transport-https 
wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
echo "deb https://packages.sury.org/php/ jessie main" | tee /etc/apt/sources.list.d/php.list

apt -y install php7.3 php7.3-cli php7.3-common php7.3-mbstring php7.3-gd php7.3-pgsql php7.3-xml php7.3-ldap 

echo "> PHP instalado " >> bitacora.log
echo "> PHP instalado "

##############################################################
##############################################################

# Instalación base en debian 8 web server

# Instalación de apache
apt-get install apache2

# Activando módulos Apache
sudo a2enmod ssl
sudo a2enmod rewrite
echo "> Apache instalado " >> bitacora.log
echo "> Apache instalado "
##############################################################
##############################################################

# Extraer archivos de respaldo

tar -xzvf respaldo.tar.gz 
cd respaldo
echo "> Extrayendo respaldo " >> bitacora.log
echo "> Extrayendo respaldo "

# Movemos certificados

mv sitio1.key /etc/ssl/private/
mv sitio2.key /etc/ssl/private/

mv sitio1.pem /etc/ssl/certs/
mv sitio2.pem /etc/ssl/certs/

# Movemos configuraciones

cp sitio1.conf /etc/apache2/sites-available/
# Añadir contenido sitio1.conf
# vim /etc/apache2/sites-available/sitio2.conf
cp sitio2.conf /etc/apache2/sites-available/
# Añadir contenido sitio2.conf

# Habilitamos sitios

echo "> Archivos movidos " >> bitacora.log
echo "> Archivos movidos "

sudo a2ensite sitio1.conf
sudo a2ensite sitio2.conf

systemctl restart apache2

echo "> Sitios habilitados " >> bitacora.log
echo "> Sitios habilitados "

##############################################################
##############################################################

# Carpetas

mkdir /var/www/sitio1
mkdir /var/www/sitio2

# Configurar /etc/hosts
vim /etc/hosts
# 127.0.1.2       sitio1.cert.unam.mx www.sitio1.cert.unam.mx

##############################################################
##############################################################

echo "> Instalando postgresql " >> bitacora.log
echo "> Instalando postgresql "

# Instalando postgres
sudo apt install -y vim wget
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
RELEASE=$(lsb_release -cs)
echo "deb http://apt.postgresql.org/pub/repos/apt/ ${RELEASE}"-pgdg main | sudo tee  /etc/apt/sources.list.d/pgdg.list

sudo apt update
sudo apt -y install postgresql-11


##############################################################
##############################################################

# Drush

echo "> Instalando Drush " >> bitacora.log
echo "> Instalando Drush "

apt-get -y install git curl

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Para verificar
# composer --version

wget https://github.com/drush-ops/drush/releases/download/8.0.1/drush.phar
# Verificación 
# php drush.phar core-status
chmod +x drush.phar
mv drush.phar /usr/local/bin/drush
drush init

# Verificación drush
# drush

##############################################################
##############################################################

# Sitio 1

echo "> Instalando Drupal " >> bitacora.log
echo "> Instalando Drupal "

cd /var/www/sitio1/
drush dl drupal-8
# Selecciona la versión estable

# Descomprime y mueve todo para que quede en /var/www/sitio1
mv drupal-8.8.4/* ./
rm -r drupal-8.8.4/

chown -R www-data:www-data /var/www/sitio1

# Instalación de sitio
drush site-install --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio1_8db --account-name=becario --account-pass=hola123., --yes

##############################################################
##############################################################

# Sitio 2

echo "> Instalando Drupal " >> bitacora.log
echo "> Instalando Drupal "

cd /var/www/sitio2/
drush dl drupal-8
# Selecciona la versión estable

# Descomprime y mueve todo para que quede en /var/www/sitio1
mv drupal-8.8.4/* ./
rm -r drupal-8.8.4/

chown -R www-data:www-data /var/www/sitio2

# Instalación de sitio
drush site-install standard --db-url=pgsql://becario:hola123.,@192.168.100.100:5432/sitio2_8db --account-name=becario --account-pass=hola123., --yes

echo "> Haciendo seguro este server ;)"
echo "> Haciendo seguro este server ;)" >> bitacora.log

sudo a2enmod rewrite 
sudo a2enmod headers

mkdir /var/www/sitio1/sites/default/files/tmp
cp ./configuraciones_seguras/.htaccess /var/www/sitio1/
cp ./configuraciones_seguras/.htaccess /var/www/sitio2/
cp ./configuraciones_seguras/sitio1.conf /etc/apache2/sites-available/
cp ./configuraciones_seguras/sitio2.conf /etc/apache2/sites-available/
cp ./configuraciones_seguras/security.conf /etc/apache2/conf-available/

echo "> Reparando permisos " >> bitacora.log
echo "> Reparando permisos "

# Verificamos permisos
cd /var/www/sitio1
sudo find . -type d -exec chmod u=rwx,g=rx,o= '{}' \;
sudo find . -type f -exec chmod u=rw,g=r,o= '{}' \;
sudo chown -R user:www-data *
chown www-data:www-data * .htaccess

# Verificamos permisos
cd /var/www/sitio2
sudo find . -type d -exec chmod u=rwx,g=rx,o= '{}' \;
sudo find . -type f -exec chmod u=rw,g=r,o= '{}' \;
sudo chown -R user:www-data *

systemctl restart apache2

echo "> Corrobore la bitácora en busca de errores"
echo "> "
echo "> Si todo ha salido bien ejecute 3check.sh" 