# Memoria técnica - LDAP - Debian

## Requisitos
```
*Tener postfix instalado*

sudo systemctl status postfix
```

### Cambio de nombre del host
```
*Cambiamos primero el nombre del host de LDAP para que conincida con el nombre del dominio que haremos*

sudo hostnamectl set-hostname drupal.osito

*Posteriormente, editamos el archivo de /etc/hosts para que pueda resolver dicho dominio y apunte hacia acá.*

sudo vi /etc/hosts

> 192.168.100.100 drupal.osito

*Reiniciamos el sistema*

sudo reboot
```

### Instalación de LDAP
```
*Verificamos que el dominio exista en /etc/hosts*

sudo vi /etc/hosts

*Instalamos LDAP*

apt-get install slapd

*Nos va a pedir ingresar una contraseña de administrador*

hola123.,

*Modulos recomendados para la instalación*

apt-get install odbc-postgresql tdsodbc unixodbc-bin

*Usamos estos packetes para añadir nodos y usuarios*

apt-get ldap-utils

*Modulos para php*

apt-get -y install php5.6-ldap openssl
```

### Confirmar configuración actual de LDAP
```
slapcat | grep dn
```

### Reconfiguración de LDAP
```
*Para reconfigurar el sitio, usamos el siguiente comando*

dpkg-reconfigure slapd

*Ingresamos*

> No
> drupal.osito
> Felizosito
> hola123.,
> MDB
> No
> Yes
```

### Confirmar configuración correcta de LDAP
```
slapcat | grep dn

slapd -h "ldap://127.0.0.1/" -u ldap -f "/etc/openldap/slapd.conf" -d 256
```

### Añadiendo regla de firewall
```
*Descargamos ufw para manejar lass reglas de firewall del server*

sudo apt-get install ufw

*Creamos las reglas de los puertos para LDAP, SMTP, HTTP y HTTPS*

sudo ufw allow 389
sudo ufw allow 587
sudo ufw allow 443
sudo ufw allow 80

*Habilitamos el firewall*

sudo ufw enable

*Corroboramos el estado de LDAP*

sudo systemctl status sldap
```

### Añadiendo el nodo de usuarios
```
*Cambiamos a la carpeta de /etc/ldap*

cd /etc/ldap/

*Creamos un archivo .ldif*

touch users.ldif

*Lo llenamos con lo siguiente:*

> dn: ou=users,dc=drupal,dc=osito
> objectClass: organizationalUnit
> ou: users

*Añadimos el módulo al tree original*

ldapadd -D "cn=admin,dc=drupal,dc=osito" -W -H ldapi:/// -f users.ldif

>>>Usamos ldapadd porque añade los nodos sin necesidad de reiniciar el servicio

*Confirmamos adición del nodo*

ldapsearch -x -b "dc=drupal,dc=osito" ou
```

### Añadiendo usuarios
```
*Cambiamos a la carpeta de /etc/ldap*

cd /etc/ldap/

*Creamos un nuevo archivo .ldif para los usuasrios que daremos de alta*

touch apache_user.ldif

*Insertamos el siguiente contenido*

> dn: cn=test,ou=users,dc=druapl,dc=osito
> objectClass: top
> objectClass: account
> objectClass: posixAccount
> objectClass: shadowAccount
> cn: test
> uid: test
> uidNumber: 10001
> gidNumber: 10001
> homeDirectory: /home/test
> userPassword: hola123.,
> loginShell: /bin/bash

*Añadir el usuario al modulo*

ldapadd -D "cn=admin,dc=drupal,dc=osito" -W -H ldapi:/// -f apache_user.ldif

*Verificamos que se haya dado de alta*

ldapsearch -x -b "ou=users,dc=drupal,dc=osito"
```