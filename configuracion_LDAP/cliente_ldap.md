Dentro del equipo que se va a unir a ldap se instala lo siguiente.
```
sudo apt install libnss-ldap libpam-ldap ldap-utils
```

Lo siguiente es ingresar la dirección ip del servidor .LDAP
```
ldap://192.168.100.100
```

Después escribimos el DN.
```
dc=drupal,dc=osito
```
Se especifica la version del protocolo. Se selecciona la versión 3 y se da enter.

Se ingresa el nombre del usuario root de LDAP.
```
cn=admin,dc=drupal,dc=osito
```

Se ingresa el password del usuario.

Se selecciona 'OK' para la configuracion de NSSwitch. Para libpam-ldap se selecciona 'YES'.

Para el login a la base de datos de LDAP se selecciona 'NO'.

Se ingreda el usuario administrador para el servidor LDAP y se escoge 'OK'.
```
cn=admin,dc=drupal,dc=osito
```
Se vuelve a ingresar el password del usuario y se selecciona 'OK'

Se modifica la configuración de '/etc/nsswitch.conf' y se modifican las siguientes líneas.
```
vim /etc/nsswitch.conf

passwd:     compat  ldap
group:      compat  ldap
shadow:     compat  ldap
```

Se configura la autenticacion y sesión de pam
```
vim /etc/pam.d/common-password
```
Se configura la siguiente línea de la siguiente manera
```
password [success=1 user_unknown=ignore default=die] pam_ldap.so try_Step 4 - Testingfirst_pass
```

Ahora se agrega 'pam_mkhomedir' en el archivo '/etc/pam.d/common-session'

```
session optional pam_mkhomedir.so skel=/etc/skel umask=077
```

Una vez guradado el archivo, hacemos un reboot a la máquina y se intenta acceder con algun usuario de LDAP.