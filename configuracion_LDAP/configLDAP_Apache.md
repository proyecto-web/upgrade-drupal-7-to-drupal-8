# Memoria técnica - LDAP - Apache

## Prerequisitos

### Busqueda estandar de Apache2
```
sudo apt-cache search apache2
```

### Instalación estandar de Apache2
```
sudo apt install apache2 apache2-utils
```

### Instalación del gestor de paquetes de PHP
```
sudo apt install -y curl wget gnupg2 ca-certificates lsb-release apt-transport-https

sudo wget https://packages.sury.org/php/apt.gpg

sudo apt-key add apt.gpg

sudo echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php7.list

sudo apt update
```

### Instalación de PHP5.6
```
sudo apt install -y php5.6 php5.6-cli php5.6-common
```

### Establecer PHP5.6 como la version por default
```
sudo update-alternatives --set php /usr/bin/php5.6
```

### Verificación de estado de los servicios
```
sudo systemctl status apache2

php -v
```

### Instalando curl
```
sudo apt install curl
```

### Modulos adicionales instalados para Apache2, Postgresql v11 y Drupal
```
*Modulos para trabajar con Apache*

sudo apt install libapache2-mod-php5.6

*Modulos necesarios para trabajar con drupal*

sudo apt install -y php5.6-pgsql php5.6-dom php5.6-gd php5.6-json php5.6-pdo php5.6-simplexml php5.6-tokenizer php5.6-xml 

*Modulos adicionales de PHP*

sudo apt -y install php5.6 php5.6-cli php5.6-common php5.6-mbstring php5.6-gd php5.6-pgsql php5.6-xml php5.6-ldap
```

## Configuración de Apache con LDAP

### Para activar el módulo de LDAP
```
sudo a2enmod ldap
```

