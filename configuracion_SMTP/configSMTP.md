# Memoria técnica - SMTP

### Cambio de nombre del host
```
*Cambiamos primero el nombre del host de LDAP para que conincida con el nombre del dominio que haremos*

sudo hostnamectl set-hostname drupal.osito

*Posteriormente, editamos el archivo de /etc/hosts para que pueda resolver dicho dominio y apunte hacia acá.*

sudo vi /etc/hosts

> 192.168.100.100 drupal.osito

*Reiniciamos el sistema*

sudo reboot
```

### Instalación de postfix
```
apt-get install postfix sasl2-bin mailutils
```

### Configuración de postfix
```
*Nos movemos a la carpeta donde reside postfix*

cd /etc/postfix/

*Creamos un archivo donde se asociarán las cuentas con sus contraseñas correspondientes y el proveedor de servicios SMTP que usan*

sudo vi sasl_passwd

> *[smtp.office365.com]:587 nombre@bec.seguridad.unam.mx:password*

*Protegemos el archivo al hacerle un hash*

sudo postmap hash:/etc/postfix/sasl_passwd

*Creamos un archivo donde se tendrán las cuentas con sus contraseñas correspondientes*

sudo vi /etc/postfix/sender_canonical

> */.+/ nombre@bec.seguridad.unam.mx:password*

*Protegemos el archivo al hacerle un hash*

sudo postmap hash:/etc/postfix/sender_canonical

*Asignamos permisos*

sudo chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db  

sudo chmod 644 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db

sudo chown root:root /etc/postfix/sender_canonical /etc/postfix/sender_canonical.db

sudo chmod 644 /etc/postfix/sender_canonical /etc/postfix/sender_canonical.db

*Copiamos el certificado por defecto al directorio de postfix*

sudo cp /etc/ssl/certs/thawte_Primary_Root_CA.pem /etc/postfix/cacert.pem

*Editamos el archivo de configuración principal de SMTP para añadir lo siguiente*

sudo vi /etc/postfix/main.cf

> inet_protocols = ipv4 
> relayhost = [smtp.office365.com]:587 
> smtp_sasl_auth_enable = yes  
> smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd  
> smtp_sasl_security_options = noanonymous
> smtp_tls_security_level = may
> sender_canonical_maps = regexp:/etc/postfix/sender_canonical  
> smtp_tls_CAfile = /etc/postfix/cacert.pem
> smtp_use_tls = yes

*Reiniciamos el servicio*

sudo service postfix restart

*Creamos un mensaje de pruebas a mandar*

vi /etc/postfix/mailtest.txt

> to: becario@bec.seguridad.unam.mx
> subject: Subject Test

*Mandando correo de prueba*

sendmail -v alan.castillo@bec.seguridad.unam < /etc/postfix/mailtest.txt 
```